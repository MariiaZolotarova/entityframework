﻿namespace CollectionsAndLinq.Models
{
    public class NameId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
