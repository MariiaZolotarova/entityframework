﻿using System;

namespace CollectionsAndLinq.Models
{
    public class ProjectDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeadLine { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}
