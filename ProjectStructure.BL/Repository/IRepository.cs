﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.BL.Repository
{
    public interface IRepository<TEntity> where TEntity: Entity
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entity);
    }
}
