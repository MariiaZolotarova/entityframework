﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BL.Context.Entity
{
    public class Team : Entity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<Project> Projects { get; set; }
    }
}
