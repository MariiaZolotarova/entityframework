﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService tasksService;

        public TasksController(ITasksService tasksService)
        {
            this.tasksService = tasksService;
        }
        [HttpGet]
        public List<Task> GetTasks()
        {
            return tasksService.GetTasks();
        }

        [HttpGet("{id}")]
        public Task GetTaskId([FromRoute] int id)
        {
            return tasksService.GetTaskId(id);
        }
        [HttpPost]
        public void CreateTask([FromBody] TaskDto task)
        {
            tasksService.CreateTask(task);
        }
        [HttpDelete("{id}")]
        public void DeleteTask([FromRoute] int id)
        {
            tasksService.DeleteTask(id);
        }
        [HttpPut("{id}")]
        public void UpdateTask([FromRoute] int id, [FromBody] TaskDto task)
        {
            tasksService.UpdateTask(id, task);
        }
    }
}
