﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService usersService;

        public UsersController(IUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpGet]
        public List<User> GetUsers()
        {
            return usersService.GetUsers();
        }

        [HttpGet("{id}")]
        public User GetUsersId([FromRoute] int id)
        {
            return usersService.GetUsersId(id);
        }

        [HttpPost]
        public void CreateUser([FromBody] UserDto user)
        {
            usersService.CreateUser(user);
        }

        [HttpDelete("{id}")]
        public void DeleteUser([FromRoute] int id)
        {
            usersService.DeleteUser(id);
        }

        [HttpPut("{id}")]
        public void UpdateUser([FromRoute] int id, [FromBody] UserDto user)
        {
            usersService.UpdateUser(id, user);
        }
    }
}
