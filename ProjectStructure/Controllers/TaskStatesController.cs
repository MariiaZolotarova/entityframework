﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        [HttpGet]
        public List<StateDto> GetUsers()
        {
            return new List<StateDto>
            {
                new StateDto
                {
                    TeamId = (int)StateEnum.Created,
                    Value = StateEnum.Created.ToString()
                },
                new StateDto
                {
                    TeamId = (int)StateEnum.Started,
                    Value = StateEnum.Started.ToString()
                },
                new StateDto
                {
                    TeamId = (int)StateEnum.Finished,
                    Value = StateEnum.Finished.ToString()
                },
                new StateDto
                {
                    TeamId = (int)StateEnum.Canceled,
                    Value = StateEnum.Canceled.ToString()
                }
            };
        }
    }
}
