﻿using AutoMapper;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;

namespace ProjectStructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserDto, User>();
            CreateMap<TeamDto, Team>();
            CreateMap<TaskDto, Task>();
            CreateMap<ProjectDto, Project>();
        }
    }
}
