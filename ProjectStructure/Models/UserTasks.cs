﻿using System.Collections.Generic;
using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.Models
{
    public class UserTasks
    {
        public List<Task> Tasks { get; set; }
        public User User { get; set; }
    }
}
