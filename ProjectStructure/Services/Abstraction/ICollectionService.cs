﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Controllers;
using ProjectStructure.Models;

namespace ProjectStructure.Services.Abstraction
{
    public interface ICollectionService
    {
        Dictionary<string, int> ShowCountProjectsWithTasks(int userId);
        List<Task> ShowDesignedTasksForSpecialUser(int userId);
        List<NameId> ShowPerformedTasksInCurrentYear(int userId);
        List<TeamUsers> ShowListOfTeamsOlderThan();
        List<UserTasks> ShowListOfUsersWithSortedTasks();
        ProjectTasks StructOfUser(int userId);
        List<ProjectInfo> StructOfProjects();
    }
}
